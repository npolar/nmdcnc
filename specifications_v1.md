Oceanography data at NPDC
=========================

> Comments like this are used throughout to indicate unclear points that must be discussed. Though, everything else is of course also subject to discussion.


Guidelines
----------

For long-term storage of oceanography data, we need to agree upon a common filetype and a common set of metadata that must be present in the data. There are some guidelines to help us:

- The [NetCDF Climate and Forecast (**CF**) Metadata Conventions](http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html) is the *de facto* standard for metadata definitions in earth sciences. It defines some important global and variable attributes and the *standard names* for variables.

- The [Attribute Convention for Data Discovery (**ACDD**)](http://wiki.esipfed.org/index.php/Attribute_Convention_for_Data_Discovery) builds upon CF and extends it by more global attributes.

- The [Directory Interchange Format (**DIF**) Standard](https://earthdata.nasa.gov/user-resources/standards-and-references/directory-interchange-format-dif-standard) is a mature metadata catalog for Earth sciences.

- The [Global Change Master Directory (**GCMD**)](https://earthdata.nasa.gov/about/gcmd/global-change-master-directory-gcmd-keywords) offers several controlled vocabularies for categorisation of data.

- The [**OceanSITES** format](http://www.oceansites.org/docs/oceansites_data_format_reference_manual.pdf) is a worldwide system of deepwater reference stations.

- [SeaDataNet](https://www.seadatanet.org) is a European metadatabase for marine data management.

- The [Norwegian Marine Data Centre (**NMDC**)](https://www.nmdc.no) defines data formats and metadata structures that shall be used to comply with them (the newest version 0.17 is not available online, although it should be) *This is the one, we should primarily obey.*

- **ISO 19115** is an extensive document behind a paywall that defines metadata for geographic information. Although it was dissmissed for our purpose because it is too complex, parts of it are considered by the others standards like CF.


Data formats
------------

NetCDF4 was agreed upon as common data format for published final data.

> How granular should the files be? One file per CTD cast? One file per transect? One file per mooring? One file per mooring instrument? One file for all moorings of a cruise?

Raw data should be uploaded “as is” under the same dataset as well. Intermediate data (e.g. Matlab files) should *not* be accepted.

> Should software that is necessary to read the raw data be delivered as well? Centrally or for each dataset? Might clash with some software’s licenses.

> Cruise reports might be uploaded as well. There is an ongoing discussion of maybe uploading cruise data to the expedition database and then linking there from the dataset.

> Comment by Ruben: “Metadata that is not essential for data usage should be excluded from the NetCDF files in my opinion. A reference to the metadata in the datacentre should suffice.” This is part of an ongoing discussion...

NPDC uses json files to store metadata and ships these metadata in addition to the actual data. To be compatible with NMDC and others, the metadata should in addition be stored in each NetCDF file itself, such that each NetCDF file is completely self-describing. This leads to multiplication of metadata, so care has to be taken to prevent different parallel versions of metadata (i.e. different metadata in json and NetCDF or between NetCDF files).

NMDC prefers (requires?) xml files for metadata. It should be possible to automatically generate the xml files from our internal json files and expose the xml files to NMDC upon request.

To allow searching through a common interface, the metadata should be compliant to a common controlled attribute vocabulary as the one suggested by NMDC (using a mixture of CF, ACDD, DIF and others). To allow a common analysis of data from NPI and other organisations, the variable names should also follow a common controlled vocabulary. CF hosts an extensive [*standard name* repository](http://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html) with such a vocabulary.

> SeaDataNet expects not the data but only metadata in the form of an XML document. If NPI researchers wish, we should be able to generate such XML files upon dataset generation and submit them to SeaDataNet. This possibility should be discussed.


User interface
--------------

> (This part was mostly adapted from Remi’s notes.)

### Web portal overview

One web portal endpoint should allow easy access to user-friendly interaction with the following API features:

- User authentication
- Uploading new data                (restricted)
- Modifying/replacing existing data (*very* restricted)
- Validating local data files       (should this be part of the web portal???**¹**)
- Searching for existing data
- Browsing through existing data
- Downloading data files
- Downloading data collections
- Downloading metadata

> **¹**Maybe rather provide a downloadable program to do local validation. This would minimize the need to upload potentially huge files.

The whole web portal should be part of the regular datasets catalogue planned for the end of 2019. That catalogue will support all the needs as specified here (except for validation of local data files).

> After a dataset has been assigned a DOI, it should not be possible for “normal users” to alter the data. If there should be serious errors, any changes should go through the admins in order to guarantee a consistent dataset also in 50 years.

> Alternatively, it might be conceivable to allow editing of the data, but then the old data should still be present and the dataset should get a version. A DOI issued for the original version should either point to that original version with a big fat comment that a newer version is available, or the DOI might point to the newest version, again with a big fat comment that the DOI actually points to an older version. Should every version get its own DOI? Is it possible to encode a version in DOIs?


### Data upload interface

Authorized users should be able to upload new data using a simple web interface. Uploaded files should be validated to ensure data consistency and quality. In the case of invalid files being uploaded, a detailed, human-readable error report should be presented to the user. Only batches of valid files should be accepted to prevent file duplication. So if a batch of files is uploaded and at least one of them contains error(s), the whole batch is rejected. A support email address should be visible at every stage. After acceptance, a DOI should be created for the uploaded dataset.

The following file types should be validated before acceptance:

- NetCDF (application/x-netcdf) following the NMDC D3.1 NetCDF specifications

Other file types should be accepted as raw data without validation. The user may either upload a whole folder or a compressed file (e.g. zip) with all the raw data in it.

> I (MB) found notes about the possibility of adding metadata separately or via link. This possibility should be discussed.

> Technicality: Can we keep uploaded files and their error reports for some time (say, a week or so)? This way, we could easily check support requests.

> Idea for (more or less) easy handling of metadata: After the metadataset is created, allow for the generation of a “custom” matlab-script to generate NetCDF files from matlab files. This script could then contain all the metadata entered in the the metadataset such that they are correctly introduced in the NetCDF file as well.


### Search interface

Users should be able to search for existing data using a simple web-interface. Since data is bound to geospatial coordinates, data should be presented in a map view. Search results should be sharable via meaningful URLs. Individual files should have URLs, whole datasets should have DOIs and URLs.

Input fields should allow the user to filter by commonly used attributes such as:
- Geospatial coordinates (filtering also by map)
- Date and time
- Cruise and station names
- Authors and contributors

Search and filtering results should be presented as:
- Persistent links to individual data files in the original format (e.g. NetCDF)
- Non-persistent links to collections of actual data (e.g. ZIP containing several NetCDF files)
- JSON containing metadata and persistent data links of entire collections
- GeoJSON containing geospatial coordinates and persistent data links of entire collections


### Download

Users should be able to download actual data using persistent URLs. Access to both individual data files and collections of data files should be supported. Download should be supported both using standard HTTP(S) and OPeNDAP for convenient access.


Global attributes
-----------------

Mostly copied from NMDC D3.1 Appendix B since it is not available online. Added some entries for OceanSITE. Sorted by name.

**Req.** states whether the attribute is *Man*datory, *Rec*ommended, *Opt*ional or *Cond*itional. Conditional attributes are further described in the comment.
**Type** indicates whether a controlled *Voc*abulary must be used for this attribute.


| Name | Req. | Type | Source | Comment |
| ---- | ---- | ---- | ------ | ------- |
| acknowledgement | Opt |  | ACDD | Free text to acknowledge various types of support. |
| ancillary_keywords | Rec |  | DIF, ACDD | Free text, comma-separated keywords to describe the dataset. |
| area | Opt | Voc | GDAC | Rough area description (e.g. “North Atlantic Ocean”) |
| cdm_data_type | Man | Voc | ACDD | The Unidata common data model data type, i.e. Grid, Image, Point, Radial, Station, Swath or Trajectory |
| citation | Rec |  | GDAC | The citation to be used in publications using the dataset. |
| comment | Opt |  | CF, ACDD | Free text with miscellaneous information about the data or methods used to produce the dataset. |
| contributor_email | Opt |  | ACDD | Semicolon-separated email addresses in accordance with `contributor_name`. |
| contributor_name | Opt |  | ACDD | A semicolon-separated list of the names of any persons or institutions that contributed. |
| contributor_role | Opt |  | ACDD | Semicolon-separated list of roles in accordance with `contributor_name`. |
| Conventions | Man | Voc | CF | Name of the conventions followed by the dataset. Normally: “CF-1.6 ACDD-1.2”, optionally with “OceanSITES-1.3” |
| data_assembly_center | Man | Voc | GDAC | Data assembly centre in charge of this data file. Use GCMD data centres. |
| data_set_language | Rec | Voc | DIF | Describes the language used in the preparation, storage, and description of the data. Should be “eng” (or maybe “nor”, “nob”, “nno”). |
| data_set_progress | Rec | Voc | DIF | Status of the data set regarding completeness. One of “planned”, “in work” or “complete” |
| data_mode | Man | Voc | OS | OceanSITE data mode: R real-time data, P provisional data, D delayed-mode data, M mixed data |
| data_type | Man | Voc | OS | OceanSITE data type. Must be “OceanSITES [profile|time-series|trajectory] data” |
| date_created | Rec | Voc | ACDD | Date on which the data file was created (ISO 8601). |
| date_modified | Rec | Voc | ACDD | The date on which the data file (not metadata!) was last changed (ISO 8601). |
| date_update | Man |  | OS | File update or creation date (OceanSITE only; ISO 8601). |
| featureType | Cond | Voc | CF | Mandatory, if a discrete sampling geometry is use. One of point, timeSeries, trajectory, profile, timeSeriesProfile or trajectoryProfile |
| format_version | Man |  | OS | OceanSITE format version. Should be “1.3” |
| geospatial_lat_max | Man |  | ACDD | Northernmost latitude in WGS-84 degrees (between -90 and 90) |
| geospatial_lat_min | Man |  | ACDD | Southernmost latitude in WGS-84 degrees (between -90 and 90) |
| geospatial_lat_resolution | Opt |  | DIF, ACDD | Latitude resolution (e.g. “100 meters” or “0.1 degree”) |
| geospatial_lat_units | Opt | Voc | ACDD | “degree_north” |
| geospatial_lon_max | Man |  | ACDD | Easternmost longitude in WGS-84 degrees (between -180 and 180) |
| geospatial_lon_min | Man |  | ACDD | Westernmost longitude in WGS-84 degrees (between -180 and 180) |
| geospatial_lon_resolution | Opt |  | DIF, ACDD | Longitude resolution (e.g. “100 meters” or “0.1 degree”) |
| geospatial_lon_units | Opt | Voc | ACDD | “degree_east” |
| geospatial_vertical_max | Man |  | ACDD | Maximum depth or height of measurement in a unit defined by `geospatial_vertical_units` |
| geospatial_vertical_min | Man |  | ACDD | Minimum depth or height of measurement in a unit defined by `geospatial_vertical_units` |
| geospatial_vertical_positive | Opt | Voc | ACDD | States which direction positive numbers point to. “down” for depth or pressure, otherwise “up”. |
| geospatial_vertical_resolution | Opt |  | DIF, ACDD | Vertical resolution |
| geospatial_vertical_units | Opt | Voc | ACDD | “meter” or other SI-compliant unit |
| history | Rec |  | ACDD | Audit trail of modifications to the file. One step per line, starting with an ISO 8601 date-time. |
| id | Man |  | ACDD | This should be a globally unique identifier for the dataset. At NPDC, we will use a [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier) |
| institution | Rec | Voc? | CF, ACDD | Institution where the original data was produced. Use free text or GCMD data centres list. (discussion, see below) |
| iso_topic_category | Man | Voc | DIF | Identification of the data type with a keyword from ISO 19115 (e.g. ocean) |
| keywords | Man | Voc | ACDD | Comma-separated list of keywords from the `keywords_vocabulary` (next row; preferably GCMD) |
| keywords_vocabulary | Man | Voc | ACDD | One of “GCMD Science Keywords”, “SeaDataNet Parameter Discovery Vocabulary”, “AGU Index Terms”; We will prefer GCMD! |
| license | Man |  | ACDD | According to NMDC a free text with explanation. At NPDC, we use license names (e.g. CC-BY 4.0). |
| multimedia_sample | Opt |  | DIF | Information about possible sample/preview images or other media. “File>URL>Format>Caption>Description” |
| naming_authority | Man |  | ACDD | The organization that manages data set names, usually “npolar.no” |
| platform | Rec | Voc? | DIF | Free text or GCMD keyword defining the platform used to gather the data. (discussion, see below) |
| platform_code | Man | Voc | OS | The OceanSITE platform code. Long list, see [here](http://seadatanet.maris2.nl/v_bodc_vocab_v2/search.asp?lib=C17). |
| private_description | Rec | Voc | DIF | If “true”, restrict the data set description from being publicly available. Should be “false”. |
| processing_level | Man | Voc | ACDD | Level of processing and quality control applied to data. Use one of the terms given [below this table](#processing_level-values). |
| project | Rec |  | DIF | Name of the scientific project that produced the data. |
| publisher_email | Rec |  | ACDD | Email address of the person responsible for metadata and formatting of the data file. |
| publisher_name | Man |  | ACDD | Name of the person responsible for metadata and formatting of the data file. |
| publisher_url | Rec |  | ACDD | Web address of the institution or the data publisher. Should be “https://data.npolar.no” |
| QC_indicator | Rec | Voc | OS | OceanSITE quality control. One of “unknown”, “excellent”, “probably good”, “mixed” |
| references | Rec |  | CF | Published or web-based references that describe the data or methods used to produce the dataset. |
| related_url | Opt |  | DIF | Links to websites containing information related to the data. |
| site_code | Man | Voc | OS | The OceanSITE site code. For us: “FRAM”; each folder [here](ftp://ftp.ifremer.fr/ifremer/oceansites/DATA) is a site code. |
| source | Rec |  | CF | Method of production of the original data (surprisingly, no controlled vocabulary!) e.g. “mooring” |
| summary | Man |  | ACDD | Free-format text describing the dataset, usually up to 100 words. |
| time_coverage_duration | Opt | Voc | ACDD | Time coverage duration in ISO 8601 (e.g. “P1M” for 1 month). |
| time_coverage_end | Man | Voc | ACDD | Date and time of last data point in UTC and in ISO 8601 format (yyyy-mm-ddThh:mm:ssZ) |
| time_coverage_resolution | Opt | Voc | ACDD | Time coverage resolution in ISO 8601 (e.g. “PT10M” for 10 minutes). |
| time_coverage_start | Man | Voc | ACDD | Date and time of first data point in UTC and in ISO 8601 format (yyyy-mm-ddThh:mm:ssZ) |
| title | Man |  | ACDD | A succinct description of what is in the dataset. |
| update_interval | Man | Voc | OS | Update interval for the file in ISO 8601 or “void” if no regular updates are planned. Mandatory by OceanSITe only; otherwise optional. |


### Often used attributes not in the NMDC recommendations

There are some attributes that were frequently used in older data (mostly seen in Framstrait 2003+). NMDC allows arbitrary attributes, but it might be an idea to standardise the attributes for our local use at NPI. The attributes that caught my (MB) attention are:

- `geographical_area` - seems to overlap with the already defined keyword `area`.
- `instrument` - explaining the instrument(s) used or “unknown”; e.g. “SBE 911plus CTD”, “Aanderaa RCM 8”, ... `instrument` is suggested by ACDD (maybe with `instrument_vocabulary` if used with a controlled vocabulary).
- `instrument_type` - the type of the instrument, e.g. “Upward Looking Sonar”, “CTD”, ...
- `location` - seems to be a more detailed description of the place on Earth than `area`. Where `area` is a rough description (“North Atlantic Ocean”), `location` is more detailed (“Kongsfjorden”). Might use the NPI placenames where possible.
- `standard_name_vocabulary` - the vocabulary used to define the `standard_name` attribute of variables ([see below](#Variable-Attributes)). Something like “CF Standard Name Table v62”.


### Unclear attributes/values

There are also some unclear attributes:

- `platform` should, according to CF, point to a GCMD controlled vocabulary. This specific vocabulary is extremely small and badly maintained. No Norwegian research vessel or station is present in that vocabulary. In our NetCDF files, usually the name of the platform was simply given (e.g. “Lance”). We might want to consider registering the various Norwegian vessels and stations in the vocabulary. Then, this would become something like “In Situ Ocean-based Platforms>SHIPS>R/V LANCE”. According to ACDD, `platform` should be accompanied by `platform_vocabulary` if a controlled vocabulary is used.
- `institution` was until now (almost) always given as “Norwegian Polar Institute (NPI)”. According to CF, this should also be a GCMD controlled vocabulary, which would (in most cases) lead to the same value as `data_assembly_center`, “GOVERNMENT AGENCIES-NON-US>NORWAY>>>NO/NPI”.


### processing_level values

Possible values of the global attribute as named in the table above.

- Raw instrument data
- Instrument data that has been converted to geophysical values
- Post-recovery calibrations have been applied
- Data has been scaled using contextual information
- Known bad data has been replaced with null values
- Known bad data has been replaced with values based on surrounding data
- Ranges applied, bad data flagged
- Data interpolated


### featureTypes

Possible values for `featureType` and explanations.

- `point` - A single data point (having no implied coordinate relationship to other points).
- `timeSeries` - A series of data points at the same spatial location with monotonically increasing times. e.g. single mooring instrument
- `profile` - An ordered set of data points along a vertical line at a fixed horizontal position and fixed time. e.g. a CTD cast
- `timeSeriesProfile` - A series of profile features at the same horizontal position with monotonically increasing times. e.g. a combination of multiple moorings or multiple instruments on the same mooring line
- `trajectoryProfile` - A series of profile features located at points ordered along a trajectory.
- `swath` - An array of data in "sensor coordinates". e.g. polar-orbiting satellite data
- `grid` - Data represented or projected on a regular or irregular grid.


Variable Attributes
-------------------

Each variable should have at least the attributes `standard_name` and `long_name`. The `standard_name` should be one of the [CF standard names](http://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html). The `long_name` may be any sensible name or description of the variable. Variables may have a `precision` attribute stating the prescision of the device used to record the data. Where appropriate, variables should have an attribute `_FillValue` to indicate missing values.

All variables that represent dimensional quantities should have the attribute `units`. The value of `units` should be recognised by the [Unidata Udunits package](https://www.unidata.ucar.edu/software/udunits). Usually, these are either common unit names (e.g. “meter” or “Celsius”) or actual units (e.g. “g kg-1” for salinity).

Variables that represent axes (usually longitude, latitude, depth/pressure and time) should have an `axis` attribute with the values X, Y, Z and T, respectively.

According to CF, time must be given in “*timeunits* since *some datetime*”, e.g. “minutes since 2010-01-01T00:00:00Z”. Absolute time points are not possible.

> OceanSITE is more demanding to variable attributes. If the discussion shows that we should fully support OceanSITE, I (MB) will list the requirements here.


Abbreviations
-------------

|      |                                             |
| ---- | ------------------------------------------- |
| ACDD | Attribute Convention for Data Discovery     |
| CF   | Climate and Forecast (Metadata Conventions) |
| DIF  | Directory Interchange Format                |
| GCMD | Global Change Master Directory              |
| GDAC | Global Data Assembly Centre                 |
| NMDC | Norwegian Marine Data Centre                |
| NPDC | Norwegian Polar Institute Data Centre       |
| NPI  | Norwegian Polar Institute                   |
| OS   | OceanSITE                                   |
