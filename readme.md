In this repository, common thoughts for oceanography data handling at NPDC are stored. Currently, everything is gathered in [specifications_v3.md](specifications_v3.md). Any comments are welcome. Please either create an issue in this repository or write an email to data æt npolar.no.

The older versions are here:

- [specifications_v3.md](specifications_v3.md)
- [specifications_v2.md](specifications_v2.md)
- [specifications_v1.md](specifications_v1.md).

A checker can be found here: [IOOS compliance checker](https://github.com/ioos/compliance-checker)

The script [fix_netCDF_to_CF_ACDD.py](fix_netCDF_to_CF_ACDD.py) turns older NetCDF files into the standard defined in [specifications_v3.md](specifications_v3.md). Example files to test the script and the expected output files are in [example_files/](example_files) and [expected_outputs/](expected_outputs), respectively. Run the script with `--help` to get more info on the usage.

The script is written in Python 3 and it need the following external packages (install e.g. with pip):

- `jdcal` for calculation in Julian date
- `netCDF4` for reading and writing NetCDF4 files
- `numpy` for handling arrays